## Mô tả : Những chức năng đã làm

-Cho phép xem chi tiết sản phẩm -> Thêm sảm phẩm vào giỏ -> Thay đổi số lượng, xóa sản phẩm -> Thanh toán(Stripe) -> Gửi mail cho khác hàng(SendGrid) -> Gửi thông báo(FCM)

-Thực hiện thêm, xóa, sửa, tìm kiếm brand, category, customer, product

-Sử dụng cloudinary để lưu ảnh

-Phân trang hiển thị cho quản lý brand, category, customer

-Thực hiện lưu thông tin các đơn hàng

-Thực hiện quản lý nhân viên và cho phép nhân viên cập nhật thông tin cá nhân

-Thực hiện lưu thông tin thanh toán

-Thực hiện lọc sản phẩm theo brand, category

## Những chức năng chưa làm được

-Chức năng chat giữa khách hàng và nhân viên
