# Mô tả :
# Những chức năng đã làm
-Cho phép xem chi tiết sản phẩm -> Thêm sảm phẩm vào giỏ -> Thay đổi số lượng, xóa sản phẩm -> Thanh toán(Stripe) -> Gửi mail cho khác hàng(SendGrid) -> Gửi thông báo(FCM)

-Thực hiện thêm, xóa, sửa, tìm kiếm brand, category, customer, product

-Sử dụng cloudinary để lưu ảnh

-Phân trang hiển thị cho quản lý brand, category, customer

-Thực hiện lưu thông tin các đơn hàng

-Thực hiện lưu thông tin thanh toán

-Thực hiện lọc sản phẩm theo brand, category

-Thực hiện tạo người dùng,... khi chạy server: migration

# Những chức năng chưa làm được
-Chức năng chat giữa khách hàng và nhân viên

# Hướng dẫn sử dụng
Bước 1: Kéo project về với lệnh : git clone https://huutai507@bitbucket.org/huutai507/huutai-fe-ecommerce.git

Bước 2: Tạo csdl với tên với page_ecommerce

Bước 3: Chuyển vào thư mục FrontEnd với lệnh:
```sh  
$ cd huutai-fe-ecommerce/FrontEnd
$ npm install
$ npm start
```

Bước 4: Chuyển vào thư mục BackEnd với lệnh:
```sh
$ cd
$ cd huutai-fe-ecommerce/BackEnd
$ npm install
$ db-migrate up
$ npm start
```
Bước 5: Truy cập vào địa chỉ http://localhost:5000/

Bước 6: Vào trang quản lý với địa chỉ : http://localhost:5000/auth/login
```sh
Tên đăng nhập : admin
Mật khẩu : 123456aA@
```